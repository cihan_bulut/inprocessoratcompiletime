package reflects;

import annotation.Macthing;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

/**
 * Created by cihanblt on 10/31/2016.
 */
public class ScanMatching {
    @Macthing(name = "test")
    public static void scanAllInMethods() throws Exception{
        Class<?> aClass = ScanMatching.class;
//        System.out.println(aClass.getName());
        Method method = aClass.getDeclaredMethod("scanAllInMethods");
        Annotation[] annotations = method.getDeclaredAnnotations();

        for (Annotation annotation : annotations) {
            if(annotation.annotationType().equals(Macthing.class)){
                Macthing macthing = (Macthing) annotation;
                System.out.println(macthing.name());
            }
        }


    }
    public static void main(String[] args) {
        try {
            scanAllInMethods();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
