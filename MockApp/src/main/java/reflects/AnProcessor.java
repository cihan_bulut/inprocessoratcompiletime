package reflects;

import annotation.Macthing;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Messager;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.tools.Diagnostic;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by cihanblt on 10/31/2016.
 */
@SupportedAnnotationTypes({"annotation.Macthing"})
public class AnProcessor extends AbstractProcessor {
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        Messager messager = processingEnv.getMessager();
        for (TypeElement t : annotations)
        {
            Map<String, Macthing> props = new LinkedHashMap<String, Macthing>();
            for (Element e : roundEnv.getElementsAnnotatedWith(t))
            {
                messager.printMessage(Diagnostic.Kind.NOTE, "Printing: " + e.getAnnotation(Macthing.class).name());
                props.put("a", e.getAnnotation(Macthing.class));
            }
        }
        return true;
    }
    @Override
    public SourceVersion getSupportedSourceVersion()
    {
        return SourceVersion.latestSupported();
    }
}
